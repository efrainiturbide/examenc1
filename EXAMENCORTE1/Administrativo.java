/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package EXAMENCORTE1;

/**
 *
 * @author efrai
 */
public class Administrativo extends Empleado{
    private String tipocontrato;

    public Administrativo(String tipocontrato, int numEmpleado, String nombre, String Domicilio, float pagodia, int diastrabajados, float impuestosisr) {
        super(numEmpleado, nombre, Domicilio, pagodia, diastrabajados, impuestosisr);
        this.tipocontrato = tipocontrato;
    }

    public Administrativo() {
        super();
        this.tipocontrato="";
    }
    
    
}
