/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package EXAMENCORTE1;

/**
 *
 * @author efrai
 */
public class Empleado {
    protected int numEmpleado;
    protected String nombre;
    protected String Domicilio;
    protected float pagodia;
    protected int diastrabajados;
    protected float impuestosisr;
//constructores
    public Empleado(int numEmpleado, String nombre, String Domicilio, float pagodia, int diastrabajados, float impuestosisr) {
        this.numEmpleado = numEmpleado;
        this.nombre = nombre;
        this.Domicilio = Domicilio;
        this.pagodia = pagodia;
        this.diastrabajados = diastrabajados;
        this.impuestosisr = impuestosisr;
    }

    public Empleado() {
        this.numEmpleado = 0;
        this.nombre = "";
        this.Domicilio = "";
        this.pagodia = 0.0f;
        this.diastrabajados = 0;
        this.impuestosisr = 0.0f;
    }

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return Domicilio;
    }

    public void setDomicilio(String Domicilio) {
        this.Domicilio = Domicilio;
    }

    public float getPagodia() {
        return pagodia;
    }

    public void setPagodia(float pagodia) {
        this.pagodia = pagodia;
    }

    public int getDiastrabajados() {
        return diastrabajados;
    }

    public void setDiastrabajados(int diastrabajados) {
        this.diastrabajados = diastrabajados;
    }

    public float getImpuestosisr() {
        return impuestosisr;
    }

    public void setImpuestosisr(float impuestosisr) {
        this.impuestosisr = impuestosisr;
    }
    public float TotalPagar(){
       return this.pagodia * this.diastrabajados;
        
    }
    public float CalcularDescuento(){
        return (float) (this.TotalPagar() * 0.12);
    }
    
    
    
    
}
