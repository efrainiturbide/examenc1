/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package EXAMENCORTE1;

/**
 *
 * @author efrai
 */
public class Docentes extends Empleado {
    private int nivel;
//constructores
    public Docentes(int nivel, int numEmpleado, String nombre, String Domicilio, float pagodia, int diastrabajados, float impuestosisr) {
        super(numEmpleado, nombre, Domicilio, pagodia, diastrabajados, impuestosisr);
        this.nivel = nivel;
    }

    public Docentes() {
        super();
        this.nivel=0;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }
    
    
}
